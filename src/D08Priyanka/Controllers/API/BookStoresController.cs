using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNet.Http;
using Microsoft.AspNet.Mvc;
using Microsoft.Data.Entity;
using D08Priyanka.Models;

namespace D08Priyanka.Controllers
{
    [Produces("application/json")]
    [Route("api/BookStores")]
    public class BookStoresController : Controller
    {
        private AppDbContext _context;

        public BookStoresController(AppDbContext context)
        {
            _context = context;
        }

        // GET: api/BookStores
        [HttpGet]
        public IEnumerable<BookStore> GetBookStores()
        {
            return _context.BookStores;
        }

        // GET: api/BookStores/5
        [HttpGet("{id}", Name = "GetBookStore")]
        public IActionResult GetBookStore([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return HttpBadRequest(ModelState);
            }

            BookStore bookStore = _context.BookStores.Single(m => m.BookStoreId == id);

            if (bookStore == null)
            {
                return HttpNotFound();
            }

            return Ok(bookStore);
        }

        // PUT: api/BookStores/5
        [HttpPut("{id}")]
        public IActionResult PutBookStore(int id, [FromBody] BookStore bookStore)
        {
            if (!ModelState.IsValid)
            {
                return HttpBadRequest(ModelState);
            }

            if (id != bookStore.BookStoreId)
            {
                return HttpBadRequest();
            }

            _context.Entry(bookStore).State = EntityState.Modified;

            try
            {
                _context.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!BookStoreExists(id))
                {
                    return HttpNotFound();
                }
                else
                {
                    throw;
                }
            }

            return new HttpStatusCodeResult(StatusCodes.Status204NoContent);
        }

        // POST: api/BookStores
        [HttpPost]
        public IActionResult PostBookStore([FromBody] BookStore bookStore)
        {
            if (!ModelState.IsValid)
            {
                return HttpBadRequest(ModelState);
            }

            _context.BookStores.Add(bookStore);
            try
            {
                _context.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (BookStoreExists(bookStore.BookStoreId))
                {
                    return new HttpStatusCodeResult(StatusCodes.Status409Conflict);
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("GetBookStore", new { id = bookStore.BookStoreId }, bookStore);
        }

        // DELETE: api/BookStores/5
        [HttpDelete("{id}")]
        public IActionResult DeleteBookStore(int id)
        {
            if (!ModelState.IsValid)
            {
                return HttpBadRequest(ModelState);
            }

            BookStore bookStore = _context.BookStores.Single(m => m.BookStoreId == id);
            if (bookStore == null)
            {
                return HttpNotFound();
            }

            _context.BookStores.Remove(bookStore);
            _context.SaveChanges();

            return Ok(bookStore);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _context.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool BookStoreExists(int id)
        {
            return _context.BookStores.Count(e => e.BookStoreId == id) > 0;
        }
    }
}