﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;

namespace D08Priyanka.Models
{
    public class AppSeedData
    {
        public static void Initialize(IServiceProvider serviceProvider)
        {

            var context = serviceProvider.GetService<AppDbContext>();
            if (context.Database == null)
            {
                throw new Exception("DB is null");
            }
                context.Locations.AddRange(
  new Location() { Latitude = 40.3494179, Longitude = -94.9238399, Place = "Maryville", State = "Missouri", Country = "USA" },
 new Location() { Latitude = 37.6991993, Longitude = -97.4843859, Place = "Wichita", State = "Kansas", Country = "USA" },
 new Location() { Latitude = 40.3494179, Longitude = -94.9238399, Place = "Maryville", State = "Missouri", Country = "USA" },
 new Location() { Latitude = 45.0993041, Longitude = -93.1007215, Place = "Whte Bear Lake", State = "Minnesota", Country = "USA" },
 new Location() { Latitude = 17.4126272, Longitude = 78.2676166, Place = "Hyderabad", Country = "India" },
new Location() { Latitude = 25.9019385, Longitude = 84.6797775, Place = "Bihar", Country = "India" }
        );
            context.BookStores.AddRange(
       new BookStore() { StoreName = "A Likely Story", ManagerName = "John", establishedyear = 1995 },
    new BookStore() { StoreName = "A Novel Idea ", ManagerName = "Jacob", establishedyear = 1992 },
    new BookStore() { StoreName = "All Booked Up ", ManagerName = "adam", establishedyear = 1993 },
    new BookStore() { StoreName = "Bookberries", ManagerName = "Jesy", establishedyear = 1994 },
 new BookStore() { StoreName = "Brillig Books", ManagerName = "tyler", establishedyear = 1996 }
                       );
            context.SaveChanges();
        }

    }
}

