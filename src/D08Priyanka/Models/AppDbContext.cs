﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Data.Entity;

namespace D08Priyanka.Models
{
    public class AppDbContext:DbContext
    {
        public DbSet<Location> Locations { get; set; }
        public DbSet<BookStore> BookStores { get; set; }
    }
}
